import React, { Component } from "react";

class Form extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="card-body">
          <h5 className="card-title">{this.props.value.prompt}</h5>
          <div className="input-group">
            <textarea
              id={this.props.value.id}
              value={this.props.value.answer}
              name="answer"
              className="form-control"
              onChange={this.props.handleAnswer.bind(this)}
            />
          </div>
          <small id={this.props.value.id} className="form-text text-muted">
            Your answer should be at least {this.props.value.min_char_length}{" "}
            character
            {this.props.value.min_char_length > 1 ? "s" : ""}. Current character
            {this.props.value.answer
              ? this.props.value.answer.length > 1
                ? "s"
                : ""
              : ""}{" "}
            count is{" "}
            {this.props.value.answer ? this.props.value.answer.length : 0}
          </small>
        </div>
      </React.Fragment>
    );
  }
}

export default Form;
