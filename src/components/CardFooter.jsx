import React, { Component } from "react";

class CardFooter extends Component {
  render() {
    const nextBtn = (
      <button className="btn btn-info" onClick={this.props.handleNext}>
        Next
      </button>
    );
    const noNextBtn = (
      <button className="btn btn-default disabled">Next</button>
    );
    const finishBtn = (
      <button className="btn btn-default" onClick={this.props.handleNext}>
        Finish
      </button>
    );
    return (
      <div className="card-footer">
        <div className="row justify-content-between">
          <div className="col-1">
            <button
              className={
                "btn btn-" +
                (this.props.currentIndex === 0 ? "default disabled" : "info")
              }
              onClick={this.props.handlePrev}
            >
              Prev
            </button>
          </div>
          <div className="col-1">
            {this.props.currentIndex < this.props.length
              ? this.props.value.answer
                ? this.props.value.answer.length >=
                  this.props.value.min_char_length
                  ? nextBtn
                  : noNextBtn
                : noNextBtn
              : finishBtn}
          </div>
        </div>
      </div>
    );
  }
}

export default CardFooter;
