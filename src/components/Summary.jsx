import React, { Component } from "react";

class Summary extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="card-body">
          <h5 className="card-title">Success!</h5>
          <ul class="list-group text-left">
            {this.props.data.questions.map((value, key) => {
              return (
                <li class="list-group-item" key={key}>
                  <h5>{value.prompt}</h5>
                  <p>{value.answer}</p>
                </li>
              );
            })}
          </ul>
        </div>
        <div className="card-footer">
          <div className="row justify-content-between">
            <div className="col-1 align-self-start">
              <button
                className={
                  "btn btn-info" +
                  (this.props.currentIndex === 0 ? "disabled" : "")
                }
                onClick={this.props.handlePrev}
              >
                Prev
              </button>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Summary;
