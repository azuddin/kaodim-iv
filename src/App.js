import React, { Component } from "react";
import "./App.css";
import data from "./data.json";
import Form from "./components/Form";
import CardFooter from "./components/CardFooter";
import Summary from "./components/Summary";

class App extends Component {
  //declaring states
  state = {
    data: data,
    length: data.questions.length,
    progress: "0",
    currentIndex: 0,
    canNext: false
  };

  //having handler method
  handleProgress = currentIndex => {
    const progress = Math.floor((currentIndex / this.state.length) * 100) + "%";
    this.setState({ progress });
  };
  handlePrev = () => {
    const currentIndex = this.state.currentIndex - 1;
    if (currentIndex >= 0) {
      this.setState({ currentIndex });
      this.handleProgress(currentIndex);
    }
  };
  handleNext = () => {
    const currentIndex = this.state.currentIndex + 1;
    this.setState({ currentIndex });
    this.handleProgress(currentIndex);
  };
  handleAnswer = event => {
    let data = this.state.data;
    data.questions = data.questions.map(value => {
      if (value.id === parseInt(event.target.id)) {
        return {
          ...value,
          answer: event.target.value
        };
      } else return { ...value };
    });
    this.setState({ data });
  };

  render() {
    return (
      <div className="App container mt-5">
        <div className="card text-center">
          <div className="card-header">
            <h2>{this.state.data.title}</h2>
            <div className="progress">
              <div
                className="progress-bar"
                role="progressbar"
                style={{ width: this.state.progress }}
              />
            </div>
          </div>
          {this.state.data.questions.map((value, key) => {
            if (key === this.state.currentIndex) {
              return (
                <React.Fragment key={value.id}>
                  <Form value={value} handleAnswer={this.handleAnswer} />
                  <CardFooter
                    value={value}
                    currentIndex={this.state.currentIndex}
                    length={this.state.length}
                    handlePrev={this.handlePrev}
                    handleNext={this.handleNext}
                  />
                </React.Fragment>
              );
            } else {
              return "";
            }
          })}
          {this.state.currentIndex === this.state.length ? (
            <Summary
              data={this.state.data}
              currentIndex={this.state.currentIndex}
              handlePrev={this.handlePrev}
            />
          ) : (
            ""
          )}
        </div>
      </div>
    );
  }
}

export default App;
